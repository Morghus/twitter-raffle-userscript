const path = require('path'),
    webpack = require('webpack'),
    stripIndent = require('common-tags').stripIndent,
    env = process.env.NODE_ENV || 'development',
    TerserPlugin = require('terser-webpack-plugin'),
    execa = require('execa');

// const gitHash = execa.sync('git', ['rev-parse', '--short', 'HEAD']).stdout;
const gitNumCommits = Number(execa.sync('git', ['rev-list', 'HEAD', '--count']).stdout);
const gitBranch = execa.sync('git', ['rev-parse', '--abbrev-ref', 'HEAD']).stdout;
const gitDirty = execa.sync('git', ['status', '-s', '-uall']).stdout.length > 0;

const version = `${gitNumCommits + (gitDirty ? 1 : 0)}${
    gitBranch === 'master' ? '' : `@${gitBranch}`
}${gitDirty ? '#dev' : ''}`;

module.exports = {
    entry: './src/index.tsx',
    devtool: env === 'production' ? false : false,
    mode: env,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, 'src'),
                use: ['style-loader', 'css-loader', 'postcss-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js', '.jsx', '.tsx'],
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'twitter-raffle.user.js',
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    format: {
                        comments: /==\/?UserScript==|^ @/,
                    },
                },
            }),
        ],
    },
    plugins: [
        new webpack.BannerPlugin({
            raw: true,
            banner: stripIndent`
                // ==UserScript==
                // @name        Twitter Raffle
                // @namespace   https://bitbucket.org/Morghus/twitter-raffle-userscript
                // @version     ${version}
                // @author      Morghus
                // @description Run a twitter raffle
                // @downloadURL https://bitbucket.org/Morghus/twitter-raffle-userscript/downloads/twitter-raffle.user.js
                // @updateURL   https://bitbucket.org/Morghus/twitter-raffle-userscript/downloads/twitter-raffle.user.js
                // @include     https://twitter.com/*
                // @run-at      document-idle
                // ==/UserScript==`,
        }),
    ],
};

import { FunctionComponent, render } from 'preact';
import { useCallback, useEffect, useState } from 'preact/hooks';
import './style.css';

function sleep(ms: number) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}

function getUsername(element: Element): string | undefined {
    const tweet = element.childNodes.item(0);
    if (tweet instanceof HTMLElement) {
        return Array.from(tweet.querySelectorAll('a div span'))
            .map(node => (node as HTMLElement).innerText)
            .filter(text => text.startsWith('@'))[0];
    }
    return undefined;
}

async function askForHashtag(winnerPanel: HTMLDivElement) {
    winnerPanel.innerHTML = `
        <h2>Enter hashtag:</h2>
        <input type="text">
        <
    `;
}

type Winner = { candidates: number; winner: { username: string; link: string } };
type UpdateState = { tweets: number; valid: number; lastUser: string };
async function raffle(
    update: (state: UpdateState) => void,
    token: { cancel: boolean }
): Promise<Winner | undefined> {
    const scrollContainer = document.querySelector('html')!;
    const main = document.querySelector('main')!;

    function scroll(pos: number) {
        scrollContainer.scrollTop = pos;
    }

    function scrollMore() {
        scrollContainer.scrollTop += 100;
    }

    function scrollToBottom() {
        scroll(99999999999);
    }

    const articles = new Map<
        string,
        { id: string; link: string; username: string; text: string }
    >();
    const tweets = new Set<string>();

    const selfUsername = /(@[A-Za-z0-9_]{4,15})/.exec(
        (document.querySelector('[data-testid="SideNav_AccountSwitcher_Button"]') as HTMLElement)
            .innerText
    )![1];

    scroll(0);
    for (;;) {
        if (document.hidden) {
            await sleep(1000);
            continue;
        }
        scrollMore();
        await sleep(45);
        if (token.cancel) {
            return;
        }
        Array.from(document.querySelectorAll('[data-testid=tweet]')).forEach(article => {
            const id = Array.from(article.querySelectorAll('a'))
                .map(a => {
                    const result = /\/status\/(\d+)$/.exec(a.getAttribute('href')!);
                    return result !== null ? result[1] : null;
                })
                .filter(id => id !== null)[0];
            if (id && !articles.has(id)) {
                // const isDirectReply = !!Array.from(
                //     article.querySelectorAll('div[dir=auto]')
                // ).filter(el => (<HTMLElement>el).innerText.startsWith('Replying to'))[0];
                // if (isDirectReply) {
                const username = getUsername(article);
                tweets.add(id);
                if (username && username.toLowerCase() !== selfUsername) {
                    const data = {
                        id,
                        link: 'https://twitter.com/' + username?.substring(1) + '/status/' + id,
                        username,
                        text: (article as HTMLElement).innerText,
                    };
                    articles.set(id, data);
                    update({ tweets: tweets.size, valid: articles.size, lastUser: username });
                }
            }
        });
        if (
            Math.ceil(scrollContainer.scrollTop) + window.innerHeight >= main.clientHeight &&
            document.querySelector('[role=progressbar]') === null
        ) {
            break;
        }
    }

    const dedupedCandidates = new Set<string>();
    const candidates = Array.from(articles.entries())
        .filter(e => e[1] !== undefined)
        .map(e => e[1])
        .filter(c => {
            if (dedupedCandidates.has(c.username)) {
                console.log('duplicate', c.username, 'removing');
                return false;
            } else {
                dedupedCandidates.add(c.username);
                return true;
            }
        });
    const winnerIndex = Math.ceil(candidates.length * Math.random()) - 1;
    const winner = candidates[winnerIndex];
    return {
        candidates: candidates.length,
        winner: { username: winner.username, link: winner.link },
    };
}

const RunModal: FunctionComponent<{ run: boolean; onClose: () => void }> = ({ run, onClose }) => {
    const [mode, setMode] = useState<undefined | 'no results' | 'error' | 'running' | 'result'>();
    const [error, setError] = useState<string>();
    const [winner, setWinner] = useState<Winner>();
    const [updateState, setUpdateState] = useState<UpdateState>();

    useEffect(() => {
        let mounted = true;
        const token = { cancel: false };
        if (run) {
            (async () => {
                if (!(await waitForElement('[data-testid="tweet"]', 10000)) && mounted) {
                    setMode('no results');
                } else if (mounted) {
                    const input = document.querySelector('[data-testid="SearchBox_Search_Input"]');
                    if (!input) {
                        setMode('error');
                        setError('Unable to find input element');
                        return;
                    }
                    setMode('running');
                    const winner = await raffle(state => mounted && setUpdateState(state), token);
                    if (mounted) {
                        setMode('result');
                        setWinner(winner);
                    }
                }
            })();
        }
        return () => {
            mounted = false;
            token.cancel = true;
        };
    }, [run, setWinner, setMode, setError]);

    return (
        <div className={`text-black modal ${run ? 'modal-open' : ''}`}>
            {run && (
                <div className="modal-box">
                    {mode === 'no results' ? (
                        <p>No results could be loaded</p>
                    ) : mode === 'error' ? (
                        <p>Error: {error}</p>
                    ) : mode === 'running' ? (
                        <div className="text-center">
                            <h3 className="text-lg">
                                <strong>{updateState?.tweets}</strong> tweets
                            </h3>
                            <h3 className="text-2xl">
                                Counted user: <strong>{updateState?.lastUser}</strong>
                            </h3>
                            <p>
                                <i>Note: Raffle will pause if the browser tab is not kept in the foreground.</i>
                            </p>
                        </div>
                    ) : mode === 'result' ? (
                        <div className="text-center">
                            <h3 className="text-lg">Counted candidates: {winner?.candidates}</h3>
                            <h2 className="text-xl">And the winner is...</h2>
                            <h1 className="text-4xl my-4">
                                <strong>{winner?.winner.username}</strong>
                            </h1>
                            <p>
                                <a className="text-blue-500" href={winner?.winner.link}>
                                    Link to tweet...
                                </a>
                            </p>
                        </div>
                    ) : (
                        <p className="text-lg text-center">Loading...</p>
                    )}
                    <div className="modal-action">
                        <button onClick={onClose} className="btn">
                            Cancel
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

const SearchModal: FunctionComponent<{
    open: boolean;
    onClose?: () => void;
    onHashtag: (hashtag: string, fromDate?: string) => void;
    onReplies: (replyLink: string, fromDate?: string) => void;
}> = ({ open, onClose, onHashtag, onReplies }) => {
    const [hashtag, setHashtag] = useState<string>();
    const [replyLink, setReplyLink] = useState<string>();
    const [fromDate, setFromDate] = useState<string>();
    const [hashtagError, setHashtagError] = useState<string>();
    const [replyLinkError, setReplyLinkError] = useState<string>();
    const [fromDateError, setFromDateError] = useState<string>();
    const [searchMode, setSearchMode] = useState<'hashtag' | 'replies'>('hashtag');

    const onModeChange = useCallback((e: Event) => {
        const mode = (e.target as HTMLInputElement).value;
        if (mode === 'hashtag' || mode === 'replies') {
            setSearchMode(mode);
        }
    }, []);

    const runRaffle = useCallback(() => {
        let error;
        if (searchMode === 'hashtag' && (!hashtag || !/^#/.test(hashtag.trim()))) {
            setHashtagError('hashtag has to start with #');
            error = true;
        } else {
            setHashtagError(undefined);
        }
        if (searchMode === 'replies' && (!replyLink || !/\b\d{5,}\b/.test(replyLink.trim()))) {
            setReplyLinkError('no tweet ID found');
            error = true;
        } else {
            setReplyLinkError(undefined);
        }
        if (fromDate && !/^\d{4}-\d{2}-\d{2}$/.test(fromDate.trim())) {
            setFromDateError('Invalid date format');
            error = true;
        } else {
            setFromDateError(undefined);
        }
        if (error) {
            return;
        }
        if (searchMode === 'hashtag') {
            onHashtag(hashtag!.trim(), fromDate?.trim());
        } else if (searchMode === 'replies') {
            const match = /\b(\d{5,})\b/.exec(replyLink!);
            onReplies(match![1], fromDate?.trim());
        }
    }, [hashtag, replyLink, fromDate, searchMode]);
    const returnFalse = useCallback((e: Event) => {
        e.preventDefault();
        return false;
    }, []);

    return (
        <div className={`modal ${open ? 'modal-open' : ''}`}>
            {open && (
                <form onSubmit={returnFalse}>
                    <div className="modal-box">
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Mode</span>
                            </label>
                            <div className="flex px-2">
                                <label
                                    className={`mr-2 cursor-pointer text-sm select-none ${
                                        searchMode === 'hashtag' ? 'text-primary' : 'text-base-300'
                                    }`}
                                    for="raffle_hashtag"
                                >
                                    Hashtag
                                </label>
                                <label
                                    for={
                                        searchMode === 'hashtag'
                                            ? 'raffle_replies'
                                            : 'raffle_hashtag'
                                    }
                                    className="relative"
                                >
                                    <input
                                        id="raffle_replies"
                                        type="radio"
                                        name="raffle_type"
                                        value="replies"
                                        checked={searchMode === 'replies'}
                                        className="toggle toggle-primary"
                                        onChange={onModeChange}
                                    />
                                    <input
                                        id="raffle_hashtag"
                                        type="radio"
                                        name="raffle_type"
                                        value="hashtag"
                                        checked={searchMode === 'hashtag'}
                                        className={
                                            searchMode === 'replies'
                                                ? 'toggle absolute top-0 left-0 z-10'
                                                : 'hidden'
                                        }
                                        style={searchMode === 'replies' ? { opacity: 0.001 } : {}}
                                        onChange={onModeChange}
                                    />
                                </label>
                                <label
                                    for="raffle_replies"
                                    className={`ml-2 text-sm cursor-pointer select-none ${
                                        searchMode === 'replies' ? 'text-primary' : 'text-base-300'
                                    }`}
                                >
                                    Replies
                                </label>
                            </div>
                        </div>
                        {searchMode === 'hashtag' && (
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">Enter hashtag</span>
                                </label>
                                <input
                                    type="text"
                                    placeholder="#hashtag"
                                    className={`w-full input ${
                                        hashtagError ? 'input-error' : 'input-primary'
                                    } input-bordered text-black`}
                                    onChange={e =>
                                        setHashtag((e?.target as HTMLInputElement).value)
                                    }
                                />
                                {hashtagError && (
                                    <label class="label">
                                        <span class="label-text-alt">{hashtagError}</span>
                                    </label>
                                )}
                            </div>
                        )}
                        {searchMode === 'replies' && (
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">
                                        Paste link to tweet (or tweet ID)
                                    </span>
                                </label>
                                <input
                                    type="text"
                                    placeholder="https://twitter.com/..."
                                    className={`w-full input ${
                                        replyLinkError ? 'input-error' : 'input-primary'
                                    } input-bordered text-black`}
                                    onChange={e =>
                                        setReplyLink((e?.target as HTMLInputElement).value)
                                    }
                                />
                                {replyLinkError && (
                                    <label class="label">
                                        <span class="label-text-alt">{replyLinkError}</span>
                                    </label>
                                )}
                            </div>
                        )}
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">(Optional) From date</span>
                            </label>
                            <input
                                type="text"
                                placeholder="yyyy-mm-dd"
                                className={`w-full input ${
                                    fromDateError ? 'input-error' : 'input-primary'
                                } input-bordered text-black`}
                                onChange={e => setFromDate((e?.target as HTMLInputElement).value)}
                            />
                            {fromDateError && (
                                <label class="label">
                                    <span class="label-text-alt">{fromDateError}</span>
                                </label>
                            )}
                        </div>
                        <div className="modal-action">
                            <button onClick={runRaffle} className="btn btn-primary">
                                Run raffle
                            </button>
                            <button onClick={onClose} className="btn">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
            )}
        </div>
    );
};

async function waitForElement(query: string, timeout: number = 2000): Promise<boolean> {
    const start = Date.now();
    for (;;) {
        if (document.querySelector(query)) {
            return true;
        }
        if (Date.now() - start > timeout) {
            break;
        }
        await sleep(100);
    }
    return false;
}

const App: FunctionComponent<{ runOnMount: boolean }> = ({ runOnMount }) => {
    const [mode, setMode] = useState<null | 'hashtag' | 'run'>(null);
    const onRaffle = useCallback(() => {
        setMode('hashtag');
    }, [setMode]);
    const closeAll = useCallback(() => {
        setMode(null);
    }, [setMode]);
    const onHashtag = useCallback(async (hashtag: string, fromDate?: string) => {
        setMode(null);
        // If this is fired then the page is not reloaded
        window.addEventListener('hashchange', () => {
            setMode('run');
            location.hash = '';
        });
        location.href =
            '/search?q=' +
            encodeURIComponent(`${hashtag}${fromDate ? ` since:${fromDate}` : ''}`) +
            '&src=typeahead_click&f=live#raffle';
    }, []);

    const onReplies = useCallback(async (conversationId: string, fromDate?: string) => {
        setMode(null);
        // If this is fired then the page is not reloaded
        window.addEventListener('hashchange', () => {
            setMode('run');
            location.hash = '';
        });
        location.href =
            '/search?q=' +
            encodeURIComponent(
                `filter:replies conversation_id:${conversationId}${
                    fromDate ? ` since:${fromDate}` : ''
                }`
            ) +
            '&src=typed_query&f=live#raffle';
    }, []);

    useEffect(() => {
        if (runOnMount) {
            setMode('run');
        }
    }, [runOnMount]);
    return (
        <div data-theme="cupcake" id="twitter-raffle-userscript">
            <button className="top-2 right-2 fixed btn btn-primary" onClick={onRaffle}>
                Raffle
            </button>
            <SearchModal
                open={mode === 'hashtag'}
                onClose={closeAll}
                onHashtag={onHashtag}
                onReplies={onReplies}
            />
            <RunModal run={mode === 'run'} onClose={closeAll} />
        </div>
    );
};

setTimeout(() => {
    const style = document.createElement('style');
    style.innerHTML = `:root { background-color: unset; }`;
    document.querySelector('head')?.appendChild(style);
    const app = document.createElement('div');
    document.body.appendChild(app);
    let runOnMount = false;
    if (location.hash === '#raffle') {
        runOnMount = true;
        location.hash = '';
    }
    render(<App runOnMount={runOnMount} />, app);
}, 100);

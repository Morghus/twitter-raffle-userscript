# Twitter raffle userscript

![Screenshot](images/screenshot.png)

## Installation

1. Install [Tampermonkey](https://www.tampermonkey.net/) ([Install for Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/) or [Install for Chome](https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo))
2. Click this link: **[twitter-raffle.user.js](https://bitbucket.org/Morghus/twitter-raffle-userscript/downloads/twitter-raffle.user.js)** (Just click, do not download, do not right-click)

## Usage

After installation, a button "Raffle" will appear on the top right of all Twitter pages in your browser. Click it to bring up the raffle dialog seen above.

Choose the mode you want (hashtag or replies) and an optional "from date" of the raffle. If you use "hashtag" mode and have run raffles with the same hashtag
previously, fill in the "from date" to include only new tweets.
